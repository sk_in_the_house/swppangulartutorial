# Installation

Run npm install -g @angular/cli, npm i -g lite-server and npm i 

# Starting for dev

ng serve -o

# Building and starting

ng build --prod

After this, a folder called "dest/xyz" has been created. Switch to this directory and run

lite-server

Your prod build is now running.